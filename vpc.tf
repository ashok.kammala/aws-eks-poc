#The VPC Creation
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name  = "custom-vpc"
  }
}

#IGW Creation
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name  = "my-igw"
  }
}

#Subnet Creation
resource "aws_subnet" "public_sn_1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.pubsn_cidr
  map_public_ip_on_launch = true
  availability_zone       = "us-east-2a"
  tags = {
    Name  = "public_sub_1"
  }
}

resource "aws_subnet" "private_sn_1" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.prisn1_cidr
  availability_zone = "us-east-2a"

  tags = {
    Name  = "private_sn_1"
  }
}

resource "aws_subnet" "private_sn_2" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.prisn2_cidr
  availability_zone = "us-east-2b"
  tags = {
    Name  = "private_sn_2"
  }
}

resource "aws_subnet" "private_sn_3" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.prisn3_cidr
  availability_zone = "us-east-2c"

  tags = {
    Name  = "private_sn_3"
  }
}

#Route table Creation
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = var.cidr_all
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name  = "public_routetable"
  }
}
resource "aws_route_table" "private_rt" {
    vpc_id = aws_vpc.vpc.id

    route {
      cidr_block = var.cidr_all
      nat_gateway_id = aws_nat_gateway.nat.id
    }
tags = {
        Name = "private_routetable"
    }
}

#Route table association with subnets
resource "aws_route_table_association" "public_rta_1" {
  subnet_id      = aws_subnet.public_sn_1.id
  route_table_id = aws_route_table.public_rt.id
}
resource "aws_route_table_association" "private_rta_1" {
  subnet_id      = aws_subnet.private_sn_1.id
  route_table_id = aws_route_table.private_rt.id
}
resource "aws_route_table_association" "private_rta_2" {
  subnet_id      = aws_subnet.private_sn_2.id
  route_table_id = aws_route_table.private_rt.id
}
resource "aws_route_table_association" "private_rta_3" {
  subnet_id      = aws_subnet.private_sn_3.id
  route_table_id = aws_route_table.private_rt.id
}

#NAT Gateway and EIP Creation
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.eip.id
  subnet_id     = aws_subnet.public_sn_1.id

  tags = {
    Name  = "nat_gw"
  }
}

resource "aws_eip" "eip" {
  tags = {
    Name  = "my_eip"
  }
}

#Security group
resource "aws_security_group" "jumphost-sg" {
   name_prefix   = "allow_tls_"
   description   = "Allow TLS inbound traffic"
   vpc_id        = aws_vpc.vpc.id

    ingress {
      description = "TLS from VPC"
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
      description = "HTTPS from VPC"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
      description = "HTTP from VPC"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  



